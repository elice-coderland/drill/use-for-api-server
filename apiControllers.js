export const getAllPosts = () => {
  return fetch('http://localhost:8080/posts')
    .then((res) => res.json());
};

export const getPostById = (id) => {
  return fetch(`http://localhost:8080/posts/${id}`)
    .then((res) => res.json());
};

export const createPost = (title, content) => {
  return fetch('http://localhost:8080/posts', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify({
      title, content
    })
  })
}

export const updatePost = (id, title, content) => {
  return fetch(`http://localhost:8080/posts/${id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      title,
      content
    })
  })
    .then((res) => {
      alert('Updated!');
    });
}

export const deletePost = (id) => {
  return fetch(`http://localhost:8080/posts/${id}`, {
    method: 'DELETE',
  })
    .then((res) => alert('DELETED!'));
};