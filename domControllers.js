import { getAllPosts, getPostById } from "./apiControllers.js";
export const showForm = (form) => {
  form.style.display = 'block';
};

export const hideForm = (form) => {
  form.style.display = 'none';
}

export const renderPosts = () => {
  getAllPosts()
    .then((posts) => {
      const postsArea = document.getElementById('posts-container');
      postsArea.innerHTML = '';

      if (!posts || posts.length === 0) {
        postsArea.innerHTML = 'No posts.';
        return;
      } else {
        posts.forEach((post) => {
          postsArea.innerHTML += `<a class="post-item" data-id=${post._id}>
          <h2>${post.title}</h2>
          <p>${post.content}</p>
          <div class="detailBtn">
            <button class="updateBtn" data-id="${post._id}">수정</button>
            <button class="deleteBtn" data-id="${post._id}">삭제</button>
          </div>
        </a>`
        })
      }
    })
}


export const editPost = (e) => {
  const postId = e.target.dataset.id;
  const postItem = document.querySelector(`.post-item[data-id="${postId}"]`);
  
  getPostById(postId)
        .then((post) => {
          const title = post.title;
          const content = post.content;

          postItem.innerHTML = `
          <input class="edit-title" value="${title}">
          <textarea class="edit-content">${content}</textarea>
          <div class="detailBtn">
            <button class="saveBtn" data-id=${postId}>저장</button>
            <button class="undoBtn">취소</button>
          </div>`;
        })
}
